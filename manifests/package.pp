class nodejs::package (
  $package_ensure  = $nodejs::params::package_ensure,
  $package_name    = $nodejs::params::package_name,
  $path            = $nodejs::params::path,
) inherits nodejs::params {
  package { $package_name:
      ensure  => $package_ensure,
      require => Class["::nodejs::prep"],
  }
}
