class nodejs::params {
  $package_ensure  = "present"
  $package_name    = "nodejs"
  $path            = "/usr/bin/"
  $node_source     = "https://deb.nodesource.com/setup_5.x"
  $node_modules    = [
    "gulp",
    "bower",
  ]
}
