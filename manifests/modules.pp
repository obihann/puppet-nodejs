class nodejs::modules (
  $package_name   = $nodejs::params::package_name,
  $package_ensure = $nodejs::params::package_ensure,
  $package_name   = $nodejs::params::package_name,
  $node_modules   = $nodejs::params::node_modules,
  $options        = '-g',
) inherits nodejs {
  package { $node_modules:
    ensure          => $package_ensure,
    provider        => "npm",
    install_options => $options,
    require         => Package[$package_name],
  }
}
