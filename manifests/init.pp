class nodejs (
  $package_ensure  = $nodejs::params::package_ensure,
  $package_name    = $nodejs::params::package_name,
  $path            = $nodejs::params::path,
  $node_source     = $nodejs::params::node_source,
) inherits nodejs::params {
  class { "::nodejs::prep":
    package_ensure => $package_ensure,
    path           => $path,
    node_source    => $node_source,
  }

  class { "::nodejs::package":
    package_ensure => $package_ensure,
    package_name   => $package_name,
    path           => $path,
  }
}
