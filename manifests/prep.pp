class nodejs::prep (
  $package_ensure  = $nodejs::params::package_ensure,
  $path            = $nodejs::params::path,
  $node_source     = $nodejs::params::node_source,
) inherits nodejs::params {
  package { "curl":
    ensure => $package_ensure
  }

  exec { "nodesource":
    command => "curl --silent --location $node_source | sudo bash -",
    path    => $path,
    require => Package["curl"],
  }
}
